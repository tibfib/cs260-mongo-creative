var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');

mongoose.connect('mongodb://lab:lab@localhost:27017/pokemon');
// mongoose.connect('mongodb://localhost:27017/comments');

var Event = mongoose.model(
    'Event',
    mongoose.Schema({
        name: String,
        value: Number
    })
);

/* GET users listing. */
router.get('/', function(req, res, next) {
    Event.find().then(function(data) {
        console.log('got comments', data);
        res
            .status(200)
            .json(data)
            .end();
    });
});

router.get('/:name', function(req, res, next) {
    Event.findOne({ name: req.params.name }).then(function(data) {
        res
            .status(200)
            .json(data)
            .end();
    });
});

router.put('/:name', function(req, res, next) {
    Event.findOneAndUpdate(
        { name: req.params.name },
        { $inc: { value: req.body.increase || 0 } },
        { new: true, upsert: true, setsDefaultOnInsert: true },
        function(err, doc) {
            if (err) {
                console.log('Something wrong when updating data!');
            }

            res.json(doc).end();
        }
    );
});
module.exports = router;
