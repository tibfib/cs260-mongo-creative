// map pollyfill just in case

//prettier-ignore
Array.prototype.map||(Array.prototype.map=function(r){var t,n,o;if(null==this)throw new TypeError("this is null or not defined");var e=Object(this),i=e.length>>>0;if("function"!=typeof r)throw new TypeError(r+" is not a function");for(arguments.length>1&&(t=arguments[1]),n=new Array(i),o=0;o<i;){var a,p;o in e&&(a=e[o],p=r.call(t,a,o,e),n[o]=p),o++}return n});

$(function() {
    console.log('loaded');

    // TIME
    var $time = $('#time');
    var amount = 1;
    function increaseTime() {
        $.ajax({ url: '/events/time', method: 'PUT', data: { increase: amount } }).done(function(
            data
        ) {
            $time.text(data.value + ' seconds');
        });
    }

    $.get('/events/time').done(function(data) {
        $time.text(data.value + ' seconds');
    });

    setInterval(increaseTime, amount * 1000);

    // CLICKS
    var $clicks = $('#clicks');
    function increaseClicks() {
        $.ajax({ url: '/events/clicks', method: 'PUT', data: { increase: 1 } }).done(function(
            data
        ) {
            $clicks.text(data.value);
        });
    }

    $.get('/events/clicks').done(function(data) {
        $clicks.text(data.value);
    });
    $(document.body).click(increaseClicks);

    // Times opened
    var $opened = $('#opened');
    function open() {
        $.ajax({ url: '/events/opened', method: 'PUT', data: { increase: 1 } }).done(function(
            data
        ) {
            $opened.text(data.value);
        });
    }
    open();
});
